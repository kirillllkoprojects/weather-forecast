import {FETCH_WEATHER} from "../actions";

export default function (state = [], action) {
    switch (action.type) {
        case FETCH_WEATHER:
            if(state.filter(cityInfo => cityInfo.city.name === action.payload.data.city.name).length > 0){
                return state;
            }
            return [action.payload.data, ...state];
    }
    return state;
}